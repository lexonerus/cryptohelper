//
//  MyTableViewCell.swift
//  CryptoHelper
//
//  Created by Alex Krzywicki on 18.01.2022.
//

import UIKit
import WebKit

class CryptoTableViewCellViewModel {
    let name:       String
    let symbol:     String
    let price:      String
    let logo_url:   String?
    var iconData:   Data?
    
    init(name: String, symbol: String, price: String, iconUrl: String) {
        self.name = name
        self.symbol = symbol
        self.price = price
        self.logo_url = iconUrl
    }
}

class CryptoTableViewCell: UITableViewCell {

    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var nameLabel:    UILabel!
    @IBOutlet var symbolLabel:  UILabel!
    @IBOutlet var priceLabel:   UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        logoImage.image     = nil
        nameLabel.text      = nil
        priceLabel.text     = nil
        symbolLabel.text    = nil
    }
    
    static let identifier = "CryptoTableViewCell"
    
    func configure(with viewModel: CryptoTableViewCellViewModel) {
        nameLabel.text = viewModel.name
        symbolLabel.text = viewModel.symbol
        priceLabel.text = viewModel.price
        
        
        if let data = viewModel.iconData {
            logoImage.image = UIImage(data: data)
        }
        else {
            if let url = URL(string: viewModel.logo_url!) {
                let task = URLSession.shared.dataTask(with: url) { [weak self] data, _, _ in
                if let data = data {
                    viewModel.iconData = data
                    DispatchQueue.main.async {
                        self!.logoImage.image = UIImage(data: data)
                    }
                }
            }
            task.resume()

            }
        }
        

    }

}
