//
//  Models.swift
//  CryptoHelper
//
//  Created by Alex Krzywicki on 18.01.2022.
//

import Foundation

struct Crypto: Codable {
    let id:         String?
    let currency:   String?
    let symbol:     String?
    let name:       String?
    let price:      String?
    let logo_url:   String?
}


/*
{
    "id": "BTC",
    "currency": "BTC",
    "symbol": "BTC",
    "name": "Bitcoin",
    "logo_url": "https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/btc.svg",
    "status": "active",
    "price": "41555.99960536",
    "price_date": "2022-01-18T00:00:00Z",
    "price_timestamp": "2022-01-18T14:54:00Z",
    "circulating_supply": "18933087",
    "max_supply": "21000000",
    "market_cap": "786783355900",
    "market_cap_dominance": "0.3824",
    "num_exchanges": "408",
    "num_pairs": "84201",
    "num_pairs_unmapped": "6557",
    "first_candle": "2011-08-18T00:00:00Z",
    "first_trade": "2011-08-18T00:00:00Z",
    "first_order_book": "2017-01-06T00:00:00Z",
    "rank": "1",
    "rank_delta": "0",
    "high": "67600.27353319",
    "high_timestamp": "2021-11-08T00:00:00Z",
    "1d": {
      "volume": "27323833445.09",
      "price_change": "-1164.12744722",
      "price_change_pct": "-0.0273",
      "volume_change": "-5307073954.83",
      "volume_change_pct": "-0.1626",
      "market_cap_change": "-22002889805.29",
      "market_cap_change_pct": "-0.0272"
    },
    "30d": {
      "volume": "999715056496.09",
      "price_change": "-5300.28776310",
      "price_change_pct": "-0.1131",
      "volume_change": "-323155226503.84",
      "volume_change_pct": "-0.2443",
      "market_cap_change": "-99037943028.11",
      "market_cap_change_pct": "-0.1118"
    }
  }
*/
